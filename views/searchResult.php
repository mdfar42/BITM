<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\BITM\SEIPXXXX\Doctor\Doctor;
use App\BITM\SEIPXXXX\User\User;
use App\BITM\SEIPXXXX\User\Auth;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;

$obj= new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

?>


<!DOCTYPE html>
<html>
<head>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../resource/assets/css/style.css">
    <style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    text-align: left;
}
</style>
</head>

<body>

<div class="container">

    <table align="center">
        <tr>
            <td height="100" >

                <div id="message" >

                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);

                    ?>
                </div>

            </td>
        </tr>
    </table>


    <header class="tab-content">

    </header>



   <article class="alert-danger">
	 <a href="test2.php">Home</a><br>
    </article>

     <h1>Manage</h1>
    <table class="table table-striped table-bordered">
  <tr>
      <th>#</th>
      <th>Name</th>
      <th>Designation</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Time</th>
      <th>Visiting Fee</th>
      <th>Action</th>
  </tr>
   <?php
   $serial = 0;
   $keyWord = $_GET['Search'];
   //echo $keyWord;
   $objDoctor = new Doctor();
   $allData=$objDoctor->searchDoctor("obj", $keyWord);

   foreach ($allData as $oneData ){
       
   ?>
       <tr>
           <td><?php echo ++$serial; ?></td>
           <td> <?php echo $oneData->name; ?></td>
           <td> <?php echo $oneData->designation; ?></td>
           <td> <?php echo $oneData->email; ?></td>
           <td> <?php echo $oneData->phone; ?></td>
           <td> <?php echo $oneData->time; ?></td>
           <td> <?php echo $oneData->visiting_fee; ?></td>
           <td> <button class="btn-success">Details</button> </td>

       </tr>
<?php  } ?>

</table>
    
</div>


<!-- Javascript -->
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");
</script>

</html>
