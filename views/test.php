<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>


    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../resource/assets/css/style.css">
    <style>  ul{
    padding: 0;
    list-style: none;
    background: #f2f2f2;
    }
    ul li{
    display: inline-block;
    position: relative;
    line-height: 21px;
    text-align: left;
    }
    ul li a{
    display: block;
    padding: 8px 25px;
    color: #333;
    text-decoration: none;
    }
    ul li a:hover{
    color: #fff;
    background: #939393;
    }
    ul li ul.dropdown{
    min-width: 100%; /* Set width of the dropdown */
    background: #f2f2f2;
    display: none;
    position: absolute;
    z-index: 999;
    left: 0;
    }
    ul li:hover ul.dropdown{
    display: block; /* Display the dropdown */
    }
    ul li ul.dropdown li{
    display: block;
    }




    </style>
</head>
<body>

<!--Header Start-->
<header class="scroll">
    <div class="container">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li>
                    <a href="#">Doctors Category &#9662;</a>
                    <ul class="dropdown">
                        <li> <a href="../views/SEIPXXXX/Patient/doctor-category.php?category=1">Dental Specialist</a></li>
                        <li><a href="../views/SEIPXXXX/Patient/doctor-category.php?category=2">Medical Specialist</a></li>
                        <li><a href="#">Cardiac Specialist </a></li>
                    </ul>
                </li>
                <li><a href="#">Contact</a></li>
            </ul>
    </div>

</header>

<!--Header Close-->



<!-- Carousel
================================================== -->



<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="../resource/Images/01.jpg" alt="ashraf">
            <div class="carousel-caption">
                Technology operates our life
            </div>
        </div>
        <div class="item">
            <img src="../resource/Images/02.jpg" alt="...">
            <div class="carousel-caption">
                Equipment is essential for better life
            </div>
        </div>
        <div class="item">
            <img src="../resource/Images/01.jpg" alt="...">
            <div class="carousel-caption">
                We are different
            </div>
        </div>

    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../resource/assets/js/jquery-1.11.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>



<!-- /.carousel -->






</body>
</html>