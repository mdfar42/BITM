<?php
if (!isset($_SESSION)) session_start();
include_once('../../vendor/autoload.php');
use App\BITM\SEIPXXXX\Doctor\Doctor;
use App\BITM\SEIPXXXX\User\User;
use App\BITM\SEIPXXXX\User\Auth;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;

$obj = new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$objDoctor = new Doctor();
$allData = $objDoctor->index("obj");


$auth = new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if (!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}





######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;


if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;



$pages = ceil($recordCount/$itemsPerPage);
$someData = $objDoctor->indexPaginator($page,$itemsPerPage);
$allData= $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################





?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link href="../../resource/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../resource/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../resource/plugins/cubeportfolio/css/cubeportfolio.min.css">
    <link href="../../resource/css/nivo-lightbox.css" rel="stylesheet"/>
    <link href="../../resource/css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css"/>
    <link href="../../resource/css/owl.carousel.css" rel="stylesheet" media="screen"/>
    <link href="../../resource/css/owl.theme.css" rel="stylesheet" media="screen"/>
    <link href="../../resource/css/animate.css" rel="stylesheet"/>
    <style>
        .table-striped > tbody > tr:nth-child(2n) > td,
        {
            background-color: lightgoldenrodyellow;
        }

        .table-striped2 > tbody > tr:nth-child(2n+1) > th {
            background-color: orange;
        }



        .animate-flicker {
            animation: fadeIn 1s infinite alternate;
        }
    </style>
</head>

<body style="background:url('../../resource/Images/adminbackkk.png') no-repeat center top #FFF">


<div id="message">

    <?php if ((array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))) {
        echo "&nbsp;" . Message::message();
    }
    Message::message(NULL);

    ?>
</div>


<header class="tab-content">

    <div class="navbar nav" style="background-color: darkred">
        <nav>
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           style="color: White; font-family: Georgia,serif;font-size: 12pt">Go to <b
                                    class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?catID=2">Admin Home</a></li>
                            <li><a href="../test2.php">User View</a></li>
                            <li><a href="add-doctor.php">Add Doctor</a></li>
                            <li><a href="index_trash.php">Trash List</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           style="color: White; font-family: Georgia,serif;font-size: 12pt">Settings <b
                                    class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="User/Authentication/logout.php"
                                   style="color: red; font-family: Georgia,serif;font-size: 12pt">Logout</a></li>

                        </ul>
                    </li>


                </ul>
            </div>
    </div>
</header>

<div class="container text-center"><h1 style="font-family: Georgia,serif">
        Hello <?php echo "$singleUser->first_name $singleUser->last_name" ?>! </h1>
    <p style="color:black;font-family: Georgia,serif"><u>Welcome to backend view</u></p>
</div>

<div class="container">

    <br><br><br>
    <h1 class="animate-flicker " style="font-size:22pt;color:darkblue;font-family: Georgia,serif">Manage</h1>
</div>
<div class="container">



















    <?php
//
//    ################## search  block 2 of 5 start ##################
//
//
//    $recordCount = count($allData);
//
//
//    if (isset($_REQUEST['Page'])) $page = $_REQUEST['Page'];
//    else if (isset($_SESSION['Page'])) $page = $_SESSION['Page'];
//    else   $page = 1;
//    $_SESSION['Page'] = $page;
//
//
//    if (isset($_REQUEST['ItemsPerPage'])) $itemsPerPage = $_REQUEST['ItemsPerPage'];
//    else if (isset($_SESSION['ItemsPerPage'])) $itemsPerPage = $_SESSION['ItemsPerPage'];
//    else   $itemsPerPage = 3;
//    $_SESSION['ItemsPerPage'] = $itemsPerPage;
//
//
//    $pages = ceil($recordCount / $itemsPerPage);
//    $someData = $objDoctor->indexPaginator($page, $itemsPerPage);
//    $allData = $someData;
//
//
//    $serial = (($page - 1) * $itemsPerPage) + 1;
//
//
//    if ($serial < 1) $serial = 1;
//    ####################### pagination code block#1 of 2 end #########################################
    ?>


<!--    <div align="left" class="container">-->
<!--        <ul class="pagination">-->
<!---->
<!--            --><?php
//
//            $pageMinusOne = $page - 1;
//            $pagePlusOne = $page + 1;
//
//
//            if ($page > $pages) Utility::redirect("manage-doctor.php?Page=$pages");
//
//            if ($page > 1) echo "<li><a href='manage-doctor.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
//
//
//            for ($i = 1; $i <= $pages; $i++) {
//                if ($i == $page) echo '<li class="active"><a href="">' . $i . '</a></li>';
//                else  echo "<li><a href='?Page=$i'>" . $i . '</a></li>';
//
//            }
//            if ($page < $pages) echo "<li><a href='manage-doctor.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";
//
//            ?>
<!---->
<!--            <select class="form-control" name="ItemsPerPage" id="ItemsPerPage"-->
<!--                    onchange="javascript:location.href = this.value;">-->
<!--                --><?php
//                if ($itemsPerPage == 1) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
//                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';
//
//                if ($itemsPerPage == 4) echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
//                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';
//
//                if ($itemsPerPage == 5) echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
//                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';
//
//                if ($itemsPerPage == 6) echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
//                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';
//
//                if ($itemsPerPage == 10) echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
//                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';
//
//                if ($itemsPerPage == 15) echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
//                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
//                ?>
<!--            </select>-->
<!--        </ul>-->
<!--    </div>-->

    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="left" class="container">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;


            if($page>$pages) Utility::redirect("manage-doctor.php?Page=$pages");

            if($page>1)  echo "<li><a href='manage-doctor.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='manage-doctor.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->

    <form class="form-inline pull-left" action="../searchResult.php" method="get">
        <div class="form-group">
            <input type="text" class="form-control" id="search" placeholder="Search Your Doctor Here" name="Search">
        </div>
    </form>
</div>

<table class="table table-striped table-striped2 table-bordered thc">
    <tr>
        <th class="text-center">Id</th>
        <th class="text-center">Name</th>
        <th class="text-center">Designation</th>
        <th class="text-center">Email</th>
        <th class="text-center">Phone</th>
        <th class="text-center">Visiting Fee</th>

        <th class="text-center">Action</th>
    </tr>
    <?php

    foreach ($allData as $oneData) {

        ?>
        <tr>
            <td><?php echo $oneData->id; ?></td>
            <td> <?php echo $oneData->name; ?></td>
            <td> <?php echo $oneData->designation; ?></td>
            <td> <?php echo $oneData->email; ?></td>
            <td> <?php echo $oneData->phone; ?></td>
            <td> <?php echo $oneData->visiting_fee; ?></td>

            <td><a href="edit.php?id=<?php echo $oneData->id ?>" class="btn btn-info" role="button"><span
                            class="glyphicon glyphicon-edit"></span> Edit</a>
                <a href="trash.php?id=<?php echo $oneData->id ?>" class="btn btn-danger" role="button"><span
                            class="glyphicon glyphicon-trash"></span> Trash</a>
                <a href="delete.php?id=<?php echo $oneData->id ?>" class="btn btn-primary" role="button"><span
                            class="glyphicon glyphicon-delete"></span> Delete</a>


            </td>
        </tr>
    <?php } ?>

</table>

</div>


<!-- Javascript -->
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");
</script>

</html>
