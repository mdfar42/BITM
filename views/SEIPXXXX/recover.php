<?php
require_once ("../../vendor/autoload.php");
use App\BITM\SEIPXXXX\Doctor\Doctor;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;
$objDoctor = new Doctor();
$objDoctor->prepare($_GET);
//Utility::dd($objDoctor);
$objDoctor->recoverDoc();

Utility::redirect("manage-doctor.php");