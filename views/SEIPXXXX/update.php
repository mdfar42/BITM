<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\BITM\SEIPXXXX\Doctor\Doctor;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;

$objDoctor = new Doctor();

$objDoctor->prepare($_POST);

$objDoctor->update();

