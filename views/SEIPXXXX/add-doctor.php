<?php
if (!isset($_SESSION)) session_start();
include_once('../../vendor/autoload.php');
use App\BITM\SEIPXXXX\User\User;
use App\BITM\SEIPXXXX\User\Auth;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;

$obj = new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$auth = new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if (!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
?>


<!DOCTYPE html>
<html>
<head>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link href="../../resource/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../resource/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../resource/plugins/cubeportfolio/css/cubeportfolio.min.css">
    <link href="../../resource/css/nivo-lightbox.css" rel="stylesheet"/>
    <link href="../../resource/css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css"/>
    <link href="../../resource/css/owl.carousel.css" rel="stylesheet" media="screen"/>
    <link href="../../resource/css/owl.theme.css" rel="stylesheet" media="screen"/>
    <link href="../../resource/css/animate.css" rel="stylesheet"/>
    <style>
        .animate-flicker {
            animation: fadeIn 1s infinite alternate;
        }
    </style>
</head>

<body style="background:url('../../resource/Images/adminbackkk.png') no-repeat center top #FFF">
<div id="message">

    <?php if ((array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))) {
        echo "&nbsp;" . Message::message();
    }
    Message::message(NULL);

    ?>
</div>

<div class="navbar nav" style="background-color: darkred">
    <ul class="nav navbar-nav pull-right">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               style="color: White; font-family: Georgia,serif;font-size: 12pt">Go to <b
                    class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="index.php?catID=2">Admin Home</a></li>
                <li><a href="../test2.php">User View</a></li>
                <li><a href="manage-doctor.php">Manage Doctor</a></li>
                <li><a href="index_trash.php">Trash List</a></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               style="color: White; font-family: Georgia,serif;font-size: 12pt">Settings <b
                    class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="User/Authentication/logout.php"
                       style="color: red; font-family: Georgia,serif;font-size: 12pt">Logout</a></li>

            </ul>
        </li>


    </ul>
</div>

<header class="tab-content">
    <h1>Hello <?php echo "$singleUser->first_name $singleUser->last_name" ?>! </h1>
    <p style="color:black;font-family: Georgia,serif"><u>Welcome to backend view</u></p>
</header>


<h1 class="animate-flicker " style="font-size:22pt;color:darkblue;font-family: Georgia,serif">Add Doctors</h1>
<form class="form-horizontal" action="store.php" method="post">
    <div class="form-group">
        <label class="control-label col-sm-2" for="fname">Doctor's Name</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="fname" name="name" placeholder="Doctor's name">
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Designation</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="designation" placeholder="Designation">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Email</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="email" placeholder="Email">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Phone Number</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="phone" placeholder="Phone Number">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Address</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="address" placeholder="Address">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Time</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="time" placeholder="Time">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Visiting Fee</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="lname" name="visiting_fee" placeholder="Visiting Fee">
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Status</label>
        <div class="col-sm-8">
            <select class="form-control" id="country" name="is_active">
                <option value="Yes">Active</option>
                <option value="No">Inactive</option>

            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="lname">Select Catagory</label>
        <div class="col-sm-8">
            <select class="form-control" id="country"
                    name="category">
                <option value="">Select Category</option>
                <option value="1">Medicine</option>
                <option value="2">Cardiology</option>
                <option value="3">Gynecologist</option>
                <option value="4">Neurologist</option>
                <option value="5">Orthopedic</option>
                <option value="6">Nefrologist</option>
                <option value="7">Pediatrician</option>
        </div>
    </div>

<div class="text-center col-md-12">
    <div class="col-sm-2"></div>
    <input class="btn btn-lg btn-success col-sm-8" type="submit" value="Add">

</div>
</form>


<!-- Javascript -->
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");
</script>

</html>
