<?php
if (!isset($_SESSION)) session_start();
include_once('../../vendor/autoload.php');
use App\BITM\SEIPXXXX\User\User;
use App\BITM\SEIPXXXX\User\Auth;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;

$obj = new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$auth = new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if (!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

?>


<!DOCTYPE html>
<html>
<head>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <style>
        .body_grad {

            background: red; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(lightyellow, lightblue, lightyellow); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(lightyellow, lightblue, lightyellow); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(lightyellow, lightblue, lightyellow); /* For Firefox 3.6 to 15 */
            background: linear-gradient(lightyellow, lightblue, lightyellow); /* Standard syntax */
        }
    </style>
</head>

<body style="background:url('../../resource/Images/adminbackk.png') no-repeat center top #FFF">

<div class="container">

    <div id="message">

        <?php if ((array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))) {
            echo "&nbsp;" . Message::message();
        }
        Message::message(NULL);

        ?>
    </div>


    <header class="tab-content">
        <h1 style="font-family: Georgia,serif">Hello <?php echo "$singleUser->first_name $singleUser->last_name" ?>! </h1>
        <p style="color:black;font-family: Georgia,serif"><u>Welcome to backend view</u></p>
    </header>

    <nav>
        <ul>
            <li class="pull-right" style="list-style: none"><a href="User/Authentication/logout.php"
                                                               style="color: red; font-family: Georgia,serif;font-size: 14pt">
                    LOGOUT </a></li>
        </ul>
    </nav>
    <div class="container">

        <a href="../test2.php">
            <button class="btn btn-primary btn-lg btn-block "
                    style="color: black; font-family: Georgia,serif; font-size: 20pt">User View
            </button>
        </a><br>
        <a href="add-doctor.php">
            <button class="btn btn-primary btn-lg btn-block"
                    style="color: black; font-family: Georgia; font-size: 20pt">Add Doctor
            </button>
        </a><br>
        <a href="manage-doctor.php">
            <button class="btn btn-primary btn-lg btn-block"
                    style="color: black; font-family: Georgia; font-size: 20pt">Manage Doctor
            </button>
        </a><br>

    </div>

</div>


<!-- Javascript -->
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");
</script>

</html>
