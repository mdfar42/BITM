<?php
namespace App\BITM\SEIPXXXX\Doctor;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;
use App\BITM\SEIPXXXX\Model\Database as DB;
use PDO;


class Doctor extends DB{
    public $table="doctor";
    public $name="";
    public $designation="";
    public $category="";
    public $email="";
    public $phone="";
    public $address="";
    public $time="";
    public $visiting_fee="";
    public $is_active="";

    public function __construct(){
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('designation',$data)){
            $this->designation=$data['designation'];
        }
        if(array_key_exists('category',$data)){
            $this->category=$data['category'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('phone',$data)){
            $this->phone=$data['phone'];
        }
        if(array_key_exists('address',$data)){
            $this->address=$data['address'];
        }
        if(array_key_exists('time',$data)){
            $this->time=$data['time'];
        }

        if(array_key_exists('visiting_fee',$data)){
            $this->visiting_fee=$data['visiting_fee'];
        }
		 if(array_key_exists('is_active',$data)){
            $this->is_active=$data['is_active'];
        }
        


        return $this;
    }





    public function store() {

        $query="INSERT INTO `doctor-information`.`doctors` (`name`, `designation`, `email`, `category`, `phone`, `address`,`time`,
        `visiting_fee`,`is_active`) 
VALUES (:name, :designation, :email, :category,:phone, :address, :time, :visiting_fee, :is_active)";

        $result=$this->conn->prepare($query);

        $result->execute(array(':name'=>$this->name,':designation'=>$this->designation,':email'=>$this->email,':category'=>$this->category,
':phone'=>$this->phone,':address'=>$this->address,':time'=>$this->time,':visiting_fee'=>$this->visiting_fee,':is_active'=>$this->is_active));
        
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function index($mode="ASSOC")
    {
        $mode = strtoupper($mode);
        $STH = $this->conn->query("SELECT * from doctors where is_active='Yes' ");
        if ($mode == "OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }

        public function index_trash($mode="ASSOC"){
        $mode = strtoupper($mode);
        $STH = $this->conn->query("SELECT * from doctors where is_active='No' ");
        
        if($mode=="OBJ")
             $STH->setFetchMode(PDO::FETCH_OBJ);
        else 
              $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();
            
        return $arrAllData;
    
    }

        public function view($fetchMode="ASSOC"){

        $STH = $this->conn->query('SELECT * from doctors where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()

        public function doctor_category($fetchMode="ASSOC"){

        $STH = $this->conn->query('SELECT * from doctors where category='.$this->category);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()

        public function update(){

        $arrData  = array($this->name,$this->designation,$this->category,$this->email,$this->phone
            ,$this->address,$this->time,$this->visiting_fee,$this->is_active);

        $sql = 'UPDATE doctors  SET name  = ?   , designation = ? , category = ?, email = ?
        , phone = ?, address = ?, time = ?, visiting_fee = ?, is_active = ? where id ='.$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('manage-doctor.php');


    }// end of update()

        public function is_delete(){


        $sql = "UPDATE doctors  SET is_active='No' where id =".$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Trashed Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Trashed Successfully!</h3></div>");

        Utility::redirect('manage-doctor.php');



    }// end of is_delete

    public function change_category(){
        $query="UPDATE `doctor-information`.`doctors` SET `category`=:category  WHERE `doctors`.`email` =:email";
        $result=$this->conn->prepare($query);
        $result->execute(array(':category'=>$this->category,':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> category has been updated  successfully.
              </div>");
           // Utility::redirect('../../../../views/SEIPXXXX/User/Profile/signup.php');
        }
        else {
            echo "Error";
        }

    }


    
    public function validTokenUpdate(){
        $query="UPDATE `doctor-information`.`doctors` SET  `time`='".'Yes'."' WHERE `doctors`.`email` =:email";
        $result=$this->conn->prepare($query);
        $result->execute(array(':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../../../views/SEIPXXXX/User/Profile/signup.php');
    }
    public function delete(){




        $sqlQuery = "DELETE from `doctors` WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been deleted Successfully!");
        }
        else{
            Message::message("Error! Data has not been deleted.");

        }



    }// end of delete()


    public function recoverDoc(){

        $sqlQuery = " UPDATE  `doctors` SET is_active ='YES' WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery);

        if($result){
            Message::message("Success! Data has been recovered Successfully!");
        }
        else{
            Message::message("Error! Data has not been recovered.");

        }



    }// end of recover()

    public function doctorsCategory($mode="ASSOC", $catID){

        $mode = strtoupper($mode);
        $STH = $this->conn->query("SELECT * from doctors where category=".$catID);

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    }

    public function searchDoctor($mode="ASSOC", $keyWord){

        $mode = strtoupper($mode);
        $sql = "SELECT * FROM `doctors` WHERE`name` LIKE '%".$keyWord."%'";
        $STH = $this->conn->query($sql);

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    }

    public function indexPaginator($page=1,$ItemsPerPage=5){
        $start = ($page-1)*$ItemsPerPage;
        $sql = "SELECT * FROM `doctors` WHERE `is_active`='YES' LIMIT $start,$ItemsPerPage";
        $STH = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        //Utility::dd($arrAllData);
        return $arrAllData;
    }


}

