-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2017 at 10:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doctor-information`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` varchar(44) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `category` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `visiting_fee` int(5) NOT NULL,
  `is_active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `designation`, `category`, `email`, `phone`, `address`, `time`, `visiting_fee`, `is_active`) VALUES
(3, 'Prof. Dr. Ujjal Kanti Das', 'FCPS,FCCP, Professot ,Medicine', '1', '-', '01812347308', 'BBMH , USTC, Foy\'s Lake ,Chittagong', 'Saturday-Wednesday (5pm-9pm)', 600, 'YES'),
(4, 'Prof. Dr. M A Hasan Chowdhury', 'FCPS(Medicine) ,MACP, FACP(America) , FRCP . ', '1', '-', '01554329460', 'CMC, Chevron ,Chittagong', 'Saturday-Wednesday (6pm-9pm) ,Sunday-Friday (10am-', 600, 'Yes'),
(5, 'Dr. Md Jashim Uddin', 'MBBS , FCPS(Medicine) .Associate Professor ,CMC', '1', '-', '01756203720 , 01739357485', 'Chevron (room no:624(2nd Floor))', 'Saturday-Thursday (5pm-10pm)', 600, 'Yes'),
(6, 'Prof. Dr. Md. Anowarul Haque Chowdhury', 'MBBS , MCPS; D.Card; MD(Cardiology)', '2', '-', '01824695452', 'Chevron  (Room no:20 (2nd Floor) )', 'Everyday (except Friday & Tuesday) Time : (11am-5p', 700, 'Yes'),
(7, 'Dr. A K M Monjur Morshed', 'MBBS; FCPS(Medicine) ; DME ; Specialist in Cardiology (USA)', '2', '-', '01756203720 , 01739357485', 'Chevron  (Room no:18 (2nd Floor) )', 'Everyday (except Friday ) Time : (5pm-8pm)', 700, 'Yes'),
(8, 'Prof. Dr. M A Rauf', 'MBBS ; FCPS ; DTCD ; D.Card ; ', '2', '-', '01995205143', 'Chevron (Room no:604 (6th Floor) )', 'Everyday (except Friday ) Time : (5pm-9pm)', 700, 'Yes'),
(9, 'Prof. Dr. Shamima Siddiqa Roji', 'FCPS.(Gyne) ; DME(UK) , Professor ; CMC', '3', '-', '01756203720 ', 'Chevron  (Room no:17 (2nd Floor) )', 'Everyday (except Friday ) Time : (3pm-6pm)', 600, 'Yes'),
(10, 'Dr. Jinat Ara Chowdhury', 'MBBS ; MCPS ; DGO ;MS(gyne) ; Gyne & Ob-Gyn Specialist', '3', '-', '01819312191', 'Chevron (Room no:601 (6th Floor) )', 'Everyday (except Friday ) Time : (6pm-9pm)', 600, 'Yes'),
(11, 'Dr. Lutfun Nahar Begom (koli)', 'MBBS ; FCPS (gyne & obs) , Associate Professor ; Chottogram Ma o Shishu Hospital', '3', '-', '01554339320', 'Chevron (Room no:606 (6th Floor) )', 'Everyday (except Friday ) Time : (3pm-6pm)', 600, 'Yes'),
(12, 'Dr. Dhiman Chowdhury', 'MBBS ; MS(Neuro Surgeon) ; Neuro & Spine Surgeon ; Associate Professor , BSMMU ,', '4', '-', '01756203720 ', 'Chevron  (Room no: (4th Floor) )', 'Friday (10am-8pm)', 1000, 'Yes'),
(13, 'Dr. S M Noman Khaled Chowdhury', 'MBBS ; MS ; Neuro Surgery (IPGMR) ; Associate Professor (NINS)', '4', '-', '01819019415', 'Chevron (Room no:630 (6th Floor) )', 'Thursday (5pm - 9 pm) ; Friday ( 9am - 9pm)', 700, 'Yes'),
(14, 'Dr. Mohammed Sanaullah Shamim', 'MBBS ; BCS ; MS(Neuro Surgeon) ; ', '4', '-', '01759150549', 'Chevron (Room no:609 (6th Floor) )', 'Everyday (except Friday ) Time : (3pm-7pm)', 700, 'Yes'),
(15, 'Dr. Md. Iqbal Hossain ', 'MBBS ; MS ; Associate Professor (Orthopedic surgery) ;Chittagong Medical College', '5', '-', '01756203720 ', 'Chevron  (Room no:19 (2nd Floor) )', 'Everyday (except Friday ) Time : (7pm-10pm)', 600, 'Yes'),
(16, 'Prof. Dr. Md. Monjurul Islam', 'MBBS ; MS (Ortho) ; Ex Professor & Head of Surgery ,Chittaging Medical College H', '5', '-', '01710929831', 'CSCR (Room No : 309 , 3rd Floor)', 'Everyday (except Friday ) Time : (10am-12pm) , (5p', 700, 'Yes'),
(17, 'Dr. Md. Somirul Islam', 'MBBS , MS (ortho) ; Orthoplasti & Orthoscopic Surgery ,Orthopedic Surgeon ; CMC', '5', '-', '01911132510', 'CSCR', '-', 700, 'Yes'),
(18, 'Dr. Munir Ahsan Khan ', 'MBBS ; MS (nefrology) ; Associate Professor ,USTC', '6', '-', '01852822868', 'Chevron (Room no:606 (6th Floor) )', 'Everyday (except Friday ) Time : (6pm-9pm)', 700, 'Yes'),
(19, 'Prof. Dr. Imran Bin younus', 'MBBS ; FCPS , FRCP (Edin) ; Internist & Nefrologist', '6', '-', '01714489341', 'CSCR (Room No : 304 , 3rd Floor)', 'Everyday (except Friday & Thursday ) Time : (10pm-', 700, 'Yes'),
(20, 'Prof. Dr. M A Kashem', 'MBBS ; MD; P.hd , Proffesor (Nefrology) ; Chittagong Medical College Hospital ', '6', '-', '-', 'CSCR (Room No : 408 , 4th Floor)', 'Everyday (except Friday ) Time : (6pm-10pm)', 700, 'Yes'),
(21, 'Prof. Dr. Mahmud A Chowdhury Arju', 'MBBS ; DCH ; FCPS(Child Specialist);  FRCP (Erdin) , Professor ,Pediatrition  , ', '7', '-', '01749075011', 'Chevron  (Room no:7 (2nd Floor) )', 'Everyday (except Friday ) Time : (5pm-10pm)', 700, 'Yes'),
(22, 'Dr. Bidyuth Bishwash', 'MBBS ; DCH ; Child Specialist', '7', '-', '2555091-92', 'Chevron  (Room no: ( 11th Floor) ', 'Everyday (except Friday ) Time : (9am-12pm)', 600, 'Yes'),
(23, 'Dr. Md. Belayet Hossain Dhali', 'MBBS ; BCS ; MD; CMC ; Consultant', '7', '-', '01819917475', 'Chevron (Room no:614 (6th Floor) )', 'Everyday (except Friday ) Time : (5pm-8pm)', 700, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(4, '', 'Admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '22', '22', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
